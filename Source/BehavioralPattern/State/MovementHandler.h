// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MoveState.h"
#include "Components/ActorComponent.h"
#include "MovementHandler.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BEHAVIORALPATTERN_API UMovementHandler : public UActorComponent
{
	GENERATED_BODY()

public:
	/** @brief Override to do initial setup. */
	virtual void BeginPlay() override;

	/** @brief Set initial state. */
	void InitialState();

	/** @brief Here we bind player inputs to our functions. */
	void BindInputs();

protected:
	/** @brief Adds new state of given type to our collection. */
	void AddState(EMoveState Type);

	/** @brief Change current state to the one of given type. */
	void ApplyState(EMoveState Type);

protected:
	/** @brief We want to know if same state calls for transition. */
	UFUNCTION()
	void OnCurrentStateChanged(EMoveState NewState);

	/** @brief Move Forward. */
	UFUNCTION()
	void OnMoveAlongX(float Value);

	/** @brief Move Sideaways. */
	UFUNCTION()
	void OnMoveAlongY(float Value);

	/** @brief Simply Rotate. */
	UFUNCTION()
    void OnRotateAroundZ(float Value);

	/** @brief Stop all movement. */
	UFUNCTION()
    void OnStop();

	/** @brief React to run button press. */
	UFUNCTION()
	void OnRunBtnPressed();

	/** @brief React to run button release. */
	UFUNCTION()
	void OnRunBtnRealesed();

private:
	/** @brief A collection of unique movement states player can be in. */
	TArray<FMoveState> States;

	/** @brief Current movement state player's in. */
	FMoveState* CurrentState;

	/** @brief Save it in case we want to restore this specific state. */
	bool bIsRunning;
};

static FMoveState* GetState(TArray<FMoveState>& States, EMoveState Type)
{
	auto Result = States.FindByPredicate([=](FMoveState& State)
	{
		return (State.GetStateType() == Type);
	});

	return Result;
}

inline void UMovementHandler::BeginPlay()
{
	Super::BeginPlay();
	InitialState();
	BindInputs();
}

inline void UMovementHandler::InitialState()
{
	AddState(EMoveState::EMS_Idle);
	CurrentState = &States.Top();
}

inline void UMovementHandler::BindInputs()
{
}

inline void UMovementHandler::AddState(EMoveState Type)
{
	auto Char = dynamic_cast<ACharacter*>(GetOwner());
	if(Char)
	{
		States.AddUnique(FMoveState(Type, dynamic_cast<APlayerController*>(Char->GetController())));
		States.Top().OnStateChanged.AddDynamic(this, &UMovementHandler::OnCurrentStateChanged);
	}
}

inline void UMovementHandler::ApplyState(EMoveState Type)
{
	CurrentState = GetState(States, Type);
	if(!CurrentState)
	{
		//Same lazy initialization taking place here.
		AddState(Type);
		CurrentState = &States.Top();
	}
}

inline void UMovementHandler::OnCurrentStateChanged(EMoveState NewState)
{
	if(NewState == EMoveState::EMS_ExhaustEnd)
	{
		if(bIsRunning)
		{
			NewState = EMoveState::EMS_Run;
		}
		else
		{
			NewState = EMoveState::EMS_Idle;
		}
	}
	else if(NewState == EMoveState::EMS_ExhaustEnd)
	{
		bIsRunning = false;
	}
	ApplyState(NewState);
}

inline void UMovementHandler::OnMoveAlongX(float Value)
{
	if(CurrentState)
	{
		CurrentState->MoveAlongX(Value);
	}
}

inline void UMovementHandler::OnMoveAlongY(float Value)
{
	if(CurrentState)
	{
		CurrentState->MoveAlongY(Value);
	}
}

inline void UMovementHandler::OnRotateAroundZ(float Value)
{
	if(CurrentState)
	{
		CurrentState->RotateAroundZ(Value);
	}
}

inline void UMovementHandler::OnStop()
{
	if(CurrentState)
	{
		CurrentState->Stop();
	}
}

inline void UMovementHandler::OnRunBtnPressed()
{
	if(CurrentState && CurrentState->GetStateType() != EMoveState::EMS_ExhaustBegin)
	{
		ApplyState(EMoveState::EMS_Run);
		bIsRunning = true;
	}
}

inline void UMovementHandler::OnRunBtnRealesed()
{
	if(CurrentState && CurrentState->GetStateType() == EMoveState::EMS_Run)
	{
		ApplyState(EMoveState::EMS_Run);
		bIsRunning = false;
	}
}
