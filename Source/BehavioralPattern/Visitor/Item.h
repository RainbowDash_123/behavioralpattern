// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SavableActor.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

UCLASS()
class BEHAVIORALPATTERN_API AItem : public AActor, public ISavableActor
{
	GENERATED_BODY()

public:
	FName GetSkinName() const { return SkinName; }
	virtual void SaveData_Implementation(UGameSaver* Saver);

private:
	FName SkinName;
	float Damage;
};