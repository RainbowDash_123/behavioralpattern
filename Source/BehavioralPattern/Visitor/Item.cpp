
#include "Item.h"
#include "GameSaver.h"

void AItem::SaveData_Implementation(UGameSaver* Saver)
{
	if(Saver)
	{
		Saver->SaveItem(this);
	}
}
