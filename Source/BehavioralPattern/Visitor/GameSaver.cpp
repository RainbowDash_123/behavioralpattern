
#include "GameSaver.h"
#include "Item.h"
#include "EngineUtils.h"
#include "MyPlayer.h"
#include "Kismet/GameplayStatics.h"
#include "SavableActor.h"

UGameSaver* UGameSaver::Get()
{
	if(!Instance)
	{
		Instance = Cast<UGameSaver>(UGameplayStatics::CreateSaveGameObject(StaticClass()));
	}

	return Instance;
}

void UGameSaver::SaveGame()
{
	auto Saver = Get();
	if(Saver)
	{
		for(FActorIterator It(Saver->GetWorld()); It; ++It)
		{
			AActor* Actor = *It;
			if (Actor->GetClass()->ImplementsInterface(USavableActor::StaticClass()))
			{
				ISavableActor::Execute_SaveData(Actor, Saver);
			}

			UGameplayStatics::AsyncSaveGameToSlot(Saver, "Save", 0);
		}
	}
}

void UGameSaver::LoadGame()
{
	Instance = Cast<UGameSaver>(UGameplayStatics::LoadGameFromSlot("Save",0));

	if(Instance)
	{
		for(FActorIterator It(Instance->GetWorld()); It; ++It)
		{
			AActor* Actor = *It;
			if (Actor->GetClass()->ImplementsInterface(USavableActor::StaticClass()))
			{
				ISavableActor::Execute_LoadData(Actor, Instance);
			}	
		}
	}
}

void UGameSaver::SaveItem(AItem* Item)
{
	if(Item)
	{
		ItemSkinName = Item->GetSkinName();
	}
}

void UGameSaver::SavePlayer(AMyPlayer* Player)
{
	if(Player)
	{
		PlayerName = Player->GetPlayerName();
		PlayerHP = Player->GetCurrentHealth();
	}
}
