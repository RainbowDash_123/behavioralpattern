// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CollectionObject.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class BEHAVIORALPATTERN_API UCollectionObject : public UObject
{
	GENERATED_BODY()

public:
	virtual class UIterator* CreateFriendsIterator(){ return nullptr; }
};
