// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Step.generated.h"

class ACharacter;

/** @brief A base attack step can be executed. */
USTRUCT(BlueprintType)
struct FStep
{
	GENERATED_BODY()

public:
	FStep() = default;
	virtual ~FStep() = default;
	
	/** @brief Execute this step onto given owner. */
	virtual void Execute(ACharacter* Owner) PURE_VIRTUAL(FStep::Execute, );
};

USTRUCT() 
struct FAttackMelee : public FStep 
{ 
	GENERATED_BODY() 

public: 
	virtual void Execute(ACharacter* Owner) override 
	{ 
		// Find melee component on owner and invoke it. 
	} 
}; 

USTRUCT() 
struct FAttackQuick : public FStep 
{ 
	GENERATED_BODY() 

public: 
	virtual void Execute(ACharacter* Owner) override 
	{ 
		// Find quick attack component on owner and invoke selected weapon usage. 
	} 
}; 

USTRUCT() 
struct FAttackPrepared : public FStep 
{ 
	GENERATED_BODY() 

public: 
	virtual void Execute(ACharacter* Owner) override 
	{ 
		// Find prepared attack component and invoke it. 
	} 
}; 

USTRUCT() 
struct FUseItem : public FStep 
{ 
	GENERATED_BODY() 

public: 
	virtual void Execute(ACharacter* Owner) override 
	{ 
		// Find inventory component and invoke selected item usage. 
	} 
}; 

USTRUCT() 
struct FUseSpell : public FStep 
{ 
	GENERATED_BODY() 

public: 
	virtual void Execute(ACharacter* Owner) override 
	{ 
		// Find melee component on owner and invoke it. 
	} 
}; 